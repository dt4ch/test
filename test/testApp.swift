//
//  testApp.swift
//  test
//
//  Created by Adrian on 06.11.21.
//

import SwiftUI

@main
struct testApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
